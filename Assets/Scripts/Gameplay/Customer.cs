using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = System.Random;

public class Customer : MonoBehaviour
{
    public bool IS_SPECIAL;
    public float moveSpeed;
    public Transform target;
    public Transform vendingMachine;
    public Transform exit;
    public bool isOrdered, isGotOrder;
    [SerializeField ] private SkinnedMeshRenderer renderer;
    [SerializeField] private Transform hairPoint;
    [SerializeField] private GameObject UIObject;
    [SerializeField] private Transform UIPoint;
    public float timer;
    private bool hasEmojiShown;
    private CustomerManager manager;
    private VendingMachine machine;
    private bool nextPosIsVending;
    private int animVariant;
    private float distance;
    private Animator anim;
    private NavMeshAgent agent;
    public bool isArrived;
    private bool isMachineBroken;
    
    void Awake()
    {
        transform.rotation = Quaternion.Euler(0, 180, 0);
        InitComps(); 
       
    }

    void InitComps()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        animVariant = UnityEngine.Random.Range(0, 2);
        if (animVariant == 2) animVariant = 1;
        anim.SetInteger("Pose",animVariant);
        UIObject.SetActive(false);
    }

    public void SetCustomerManager(CustomerManager _manager)
    {
        manager = _manager;
    }

    public void InitCustomer(Transform _vendingMachine, Transform _exit,VendingMachine _machine)
    {
        vendingMachine = _vendingMachine;
        exit = _exit;
        machine = _machine;
    }
    
    public void SetCustomerDesign(GameObject _hair, Texture2D _texture)
    {
        GameObject hair = Instantiate(_hair, hairPoint.position, hairPoint.rotation,hairPoint);
        renderer.material.mainTexture = _texture;
    }
    
    public void SetCustomerTransform(Transform _trans,bool isVending)
    { 
        target = _trans;
        agent.speed = moveSpeed;
        agent.SetDestination(_trans.position);
        nextPosIsVending = isVending;
    }
    
    void HandleDistance()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance >= 2f && !nextPosIsVending  )
        {
            if (Time.frameCount % 15 == 0)
            {
                agent.SetDestination(target.position);
            }

            isArrived = false;
            anim.SetBool("Movement",true);
            anim.SetBool("Order",false);
            agent.speed = moveSpeed;
        }
        else if (distance < 2f && !nextPosIsVending)
        {
            agent.speed = 0;
            agent.velocity = Vector3.zero;
            anim.SetBool("Movement",false);
            anim.SetBool("Order",false);
            isArrived = true;
        }
        
        if (distance >= 1 && nextPosIsVending)
        {
            if (Time.frameCount % 15 == 0)
            {
                agent.SetDestination(vendingMachine.position);
            }
            anim.SetBool("Movement",true);
            isArrived = false;
            agent.speed = moveSpeed;
        }
        else if (distance < 1 && nextPosIsVending)
        {
            agent.speed = 0;
            agent.velocity = Vector3.zero;
            anim.SetBool("Movement",false);
            isArrived = true;
            if (machine.currentStack > 0 && !isMachineBroken)
            {
                anim.SetBool("Movement",false);
                anim.SetBool("Order",true);
                Invoke("GotTheOrder",2f);
            }
            else
            {
                anim.SetBool("Movement",false);
                anim.SetBool("Order",false);
            }
         
            
        }
        
    }
    

    private void LateUpdate()
    {
        HandleDistance();
        HandleUI();
        HandleTimer();
    }

    void HandleTimer()
    {
        timer += 1 * Time.deltaTime;
        if (timer > 12 && !hasEmojiShown)
        {
            hasEmojiShown = true;
            timer = 0;
           HandleEmoji(true);
        }
    }

    void HandleEmoji(bool isAngry)
    {
        int rd = 0;
        if (!isAngry)
        {
            rd = UnityEngine.Random.Range(0, 2);
        }
        else
        {
            rd = UnityEngine.Random.Range(2, 4);
        }

        UIObject.SetActive(true);
        UIObject.transform.DOShakeScale(0.25f, 0.25f, 1);
        UIObject.transform.GetChild(0).GetComponent<Image>().sprite = GameManager.Instance.emojis[rd];
        Invoke("ResetEmoji",2.5f);
    }

    void ResetEmoji()
    {
        UIObject.SetActive(false);
        // hasEmojiShown = false;
    }
    void HandleUI()
    {
        Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
        UIObject.transform.position = UIPos;
        
        if (Time.frameCount % 3 == 0)
        {
            UIObject.SetActive(!GameManager.Instance.isSecondCameraActive);
        }
    }
    
    public void GotTheOrder()
    {
        if (!isGotOrder)
        {
            HandleEmoji(false);
            isGotOrder = true;
            machine.CustomerBoughtItem();
            nextPosIsVending = false;
            SetCustomerTransform(exit,false);
            manager.RemoveCustomer(this);
            timer = 0;
            ResetEmoji();
            Destroy(gameObject,10);
        }
       
    }

    public void SetMachineBroke(bool _value)
    {
        isMachineBroken = _value;
        SetCustomerTransform(exit,false);
        HandleEmoji(true);
        isGotOrder = true;
        manager.RemoveCustomer(this);
        timer = 0;
        Destroy(gameObject,10);
    }
    
}
