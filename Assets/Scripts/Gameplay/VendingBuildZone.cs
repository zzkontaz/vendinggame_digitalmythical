using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class VendingBuildZone : MonoBehaviour
{
   public int vendingMachineType;
   public int itemID;
   public int machineID;
   public bool isRightSideCashZone;
   private bool isPoppedUp = false;
   private Transform player;
   public int  playerDistanceToCashFlow ,playerDistanceToPopUp;
   private bool isBought;
   public GameObject UIObject;
   public Transform UIPoint;
   public Image ItemImg;
   public TextMeshProUGUI MoneyText;
   [SerializeField] private GameObject[] vending;
   [SerializeField] private bool[] vendingDone;
   [SerializeField] private GameObject vendingTextObject;
   private int moneyNeeded;
   private int totalNeeded;
   private float cashRate;
   private float nextMoney;
   private float nextHaptic;


   private void Start()
   {
    Init(); 
   }
   
   private void LateUpdate()
   {
      HandlePlayerDistance();
      HandleUI();
   }

   void Init()
   {
      player = GameObject.FindGameObjectWithTag("Player").transform;
      moneyNeeded = GameManager.Instance.vendingMachineBuildCost[itemID];
      totalNeeded = moneyNeeded;
      SetMoneyText(moneyNeeded);
      cashRate = 0.5f;
      UIObject.transform.localScale = new Vector3(0.5f,0.5f,0.5f);

      for (int i = 0; i < vending.Length; i++)
      {
      vending[i].SetActive(false);         
      }
   }

   void HandleUI()
   {
      Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
      UIObject.transform.position = UIPos;

      if (Time.frameCount % 3 == 0)
      {
         UIObject.SetActive(!GameManager.Instance.isSecondCameraActive);
      }
   }

   public void OnValidate()
   {
      HandleUI();
   }

 
   void HandlePlayerDistance()
   {
      if (Time.frameCount % 2 == 0)
      {
         float distance = Vector3.Distance(player.position, transform.position);
         
         if (distance < 7)
         {
            HandleDistancePopUp(distance);
            HandleDistanceCashFlow(distance);
         }
      }
   }

   void HandleDistanceCashFlow(float distance)
   {
      if (distance < playerDistanceToCashFlow && moneyNeeded <= PlayerHandler.Instance.cash && moneyNeeded>0)
      {
         if (Time.time > nextMoney)
         {
            nextMoney = cashRate + Time.time;
            
            cashRate = cashRate / 2;
            cashRate = Mathf.Clamp(cashRate, 0.05f, 0.25f);
            moneyNeeded--;
            PlayerHandler.Instance.RemoveCash(1);
            MMVibrationManager.Haptic(HapticTypes.SoftImpact);
            GameObject _cash = Instantiate(PrefabsManager.Instance.CashGivenPrefab, transform.position,
               Quaternion.identity);
            _cash.GetComponent<Cash>().GoToTarget(this.transform,cashRate * 2f);
            SetMoneyText(moneyNeeded);
            CheckBuildingVisual();
            if (moneyNeeded <= 0 && !isBought)
           {
              isBought = true;
              BuildVending();
           }
         }
         
      }
   }
   
   
   void BuildVending()
   {
      GameObject _vendingPrefab = PrefabsManager.Instance.VendingMachinePrefabs[vendingMachineType];
      GameObject _vendingMachine = Instantiate(_vendingPrefab, transform.position, transform.rotation);
      _vendingMachine.GetComponent<VendingMachine>().SetID(vendingMachineType,itemID,machineID,isRightSideCashZone);
      gameObject.SetActive(false);
      Destroy(this.gameObject,1);
      if (GameManager.Instance.isFirstPlay)
      {
         GameManager.Instance.SetTutorialText("Go Get Orders",true);
         PlayerHandler.Instance.SetWaypoint(GameManager.Instance.tutorialTransforms[2],true);
      }
   }
   void CheckBuildingVisual()
   {
      vendingTextObject.SetActive(false);
      
      if (moneyNeeded <= totalNeeded / 1.25f && !vendingDone[0])
      {
         vending[0].SetActive(true);
         vending[0].transform.DOPunchScale(Vector3.up * 1, 0.25f,2);
         vendingDone[0] = true;
      }
      
      if (moneyNeeded <= totalNeeded / 2f && !vendingDone[1])
      {
         vending[1].SetActive(true);
         vending[1].transform.DOPunchScale(Vector3.up * 2, 0.25f,2);
         vendingDone[1] = true;
      }
      
      if (moneyNeeded ==0 && !vendingDone[2])
      {
         vending[2].SetActive(true);
         vending[2].transform.DOPunchScale(Vector3.up * 1, 0.25f,1);
         vendingDone[2] = true;
      }
      
   }
   
   void HandleDistancePopUp(float distance)
   {
      if (distance < playerDistanceToPopUp && !isPoppedUp)
      {
         isPoppedUp = true;
         UIObject.transform.DOScale(new Vector3(1, 1, 1), 0.25f);
      }
      else if(distance >= playerDistanceToPopUp && isPoppedUp)
      {
         isPoppedUp = false;
         UIObject.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.25f);
      }
   }

   void SetMoneyText(int _value)
   {
      MoneyText.text = _value.ToString();
   }
   
}
