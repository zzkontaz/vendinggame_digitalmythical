using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Door : MonoBehaviour
{
   private Transform playerTransform;
   [SerializeField] private GameObject rightDoor, leftDoor;
   private bool isOpen = false;
   
   private void Awake()
   {
      DOTween.Init();
      playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
   }

   private void LateUpdate()
   {
      HandleDistance();
   }

   void HandleDistance()
   {
      if (Time.frameCount % 1 == 0)
      {
         float distance = Vector3.Distance(playerTransform.position, transform.position);
         if (distance < 2.5f && !isOpen)
         {
            isOpen = true;
            rightDoor.transform.DOLocalRotate(new Vector3(0, -90, 0), 0.5f);
            leftDoor.transform.DOLocalRotate(new Vector3(0, 90, 0), 0.5f);
         }
         else if (distance >= 2.5f && isOpen)
         {
            isOpen = false;
            rightDoor.transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
            leftDoor.transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
         }
      }
   }
}
