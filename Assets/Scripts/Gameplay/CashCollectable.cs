using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CashCollectable : MonoBehaviour
{
  
   private bool isCollected = false;


   void PlayerCollect()
   {
      isCollected = true;
      PlayerHandler.Instance.AddCash(1);
      gameObject.SetActive(false);
      Destroy(this.gameObject,0.15f);
   }

   private void OnTriggerEnter(Collider other)
   {
      if (other.CompareTag("Player"))
      {
         if (!isCollected) PlayerCollect();
      }
   }
}
