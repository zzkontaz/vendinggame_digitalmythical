using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyOffsideHandler : MonoBehaviour
{
   private void OnTriggerEnter(Collider other)
   {
      if (other.CompareTag("CANDY"))
      {
         CandyFillingHandler.Instance.RemoveItemInMachine();
         Destroy(other.gameObject);
      }

      if (other.CompareTag("TOY"))
      {
         //
      }
   }
}
