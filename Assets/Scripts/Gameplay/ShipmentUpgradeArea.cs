using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class ShipmentUpgradeArea : MonoBehaviour
{
    [SerializeField] private GameObject UIObject;
    [SerializeField] private Transform UIPoint;
    [SerializeField] private GameObject BuyUIObject;
    [SerializeField] private GameObject DoneObject;
    [SerializeField] private GameObject MaxedOutObject;
    [SerializeField] private GameObject UpgradeArrowObject;
    [SerializeField] private TextMeshProUGUI MoneyText;
    [SerializeField] private ShipmentZone _zone;
    private bool isMaxedOut;
    [SerializeField] private int upgradeCount;
    private bool isPoppedUp = false;
    public float  playerDistanceToCashFlow ,playerDistanceToPopUp;
    private bool isBought;
    private Transform player;
    private int moneyNeeded;
    [SerializeField] int totalNeeded;
    private float cashRate;
    private float nextMoney;
    private float nextHaptic;

    private void Awake()
    {
        DOTween.Init();
    }

    private void Start()
    {
        Init();
        ResetMoneyFlow();
    }

    void Init()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        BuyUIObject.SetActive(true);
        DoneObject.SetActive(false);
        MaxedOutObject.SetActive(false);
    }
    void HandleUI()
    {
        Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
        UIObject.transform.position = UIPos;
    }

  
  

    private void LateUpdate()
    {
        HandleUI();
        HandlePlayerDistance();
    }
    
    void HandlePlayerDistance()
    {
        if (Time.frameCount % 2 == 0)
        {
            float distance = Vector3.Distance(player.position, transform.position);
         
            if (distance < 10)
            {
                HandleDistancePopUp(distance);
                HandleDistanceCashFlow(distance);
            }
        }
    }
    
    void HandleDistancePopUp(float distance)
    {
        if (distance < playerDistanceToPopUp && !isPoppedUp)
        {
            isPoppedUp = true;
            UIObject.transform.DOScale(new Vector3(1, 1, 1), 0.25f);
        }
        else if(distance >= playerDistanceToPopUp && isPoppedUp)
        {
            isPoppedUp = false;
            UIObject.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.25f);
        }
    }
    
    void HandleDistanceCashFlow(float distance)
    {
        if (distance < playerDistanceToCashFlow && moneyNeeded <= PlayerHandler.Instance.cash && moneyNeeded>0 && !isBought && !isMaxedOut)
        {
            if (Time.time > nextMoney)
            {
                nextMoney = cashRate + Time.time;
            
                cashRate = cashRate / 2;
                cashRate = Mathf.Clamp(cashRate, 0.05f, 0.25f);
                moneyNeeded--;
                PlayerHandler.Instance.RemoveCash(1);
                MMVibrationManager.Haptic(HapticTypes.SoftImpact);
                GameObject _cash = Instantiate(PrefabsManager.Instance.CashGivenPrefab, transform.position,
                    Quaternion.identity);
                _cash.GetComponent<Cash>().GoToTarget(this.transform,cashRate * 2f);
                SetMoneyText(moneyNeeded);
               
                if (moneyNeeded <= 0 && !isBought)
                {
                    isBought = true;
                   UpgradeShipment();
                }
            }
        }
    }

    void UpgradeShipment()
    {
        BuyUIObject.SetActive(false);
        DoneObject.SetActive(true);
        MaxedOutObject.SetActive(false);
        _zone.UpgradeBoxCount();
        UpgradeArrowObject.transform.DOPunchScale(Vector3.up * 5, 0.75f, 10, 1);
        upgradeCount++;
        if (upgradeCount == 3)
        {
          MaxedOut();
        }
       Invoke("ResetMoneyFlow",1.75f);
    }

    void ResetMoneyFlow()
    {
        moneyNeeded = totalNeeded;
        cashRate = 0.25f;
        SetMoneyText(moneyNeeded);
        BuyUIObject.SetActive(true);
        DoneObject.SetActive(false);
        MaxedOutObject.SetActive(false);
        isBought = false;
    }
    void SetMoneyText(int _value)
    {
        MoneyText.text = _value.ToString();
    }

    void MaxedOut()
    {
        isMaxedOut = true;
        BuyUIObject.SetActive(false);
        DoneObject.SetActive(false);
        MaxedOutObject.SetActive(true);
     
    }
}
