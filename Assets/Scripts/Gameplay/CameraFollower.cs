using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
   private Transform target;
   public Transform player;
   public Transform customerRegisterPoint;
   public Vector3 camOffset;
   public float camFollowSpeed;
   public float[] minX, maxX, minZ, maxZ;
   public static CameraFollower Instance;
   private void Awake()
   {
      Instance = this;
   }

   private void Start()
   {
      target = player;
   }

   private void LateUpdate()
   {
      transform.position = Vector3.Lerp(transform.position,target.position + camOffset, camFollowSpeed * Time.deltaTime);
      float X = transform.position.x;
      float Z = transform.position.z;
      X = Mathf.Clamp(transform.position.x, minX[GameManager.Instance.Level -1], maxX[GameManager.Instance.Level - 1]);
      Z = Mathf.Clamp(transform.position.z, minZ[GameManager.Instance.Level - 1], maxZ[GameManager.Instance.Level -1]);
      transform.position = new Vector3(X,transform.position.y,Z);
   }
   

   public void SetPlayerCamera()
   {
      target = player;
      camFollowSpeed = 4;
      PlayerHandler.Instance.canMove = true;
   }
   

   void SetNormalSpeed()
   {
      camFollowSpeed = 2;
   }
}
