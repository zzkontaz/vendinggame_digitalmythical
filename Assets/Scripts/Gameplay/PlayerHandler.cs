using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using EZCameraShake;
using MoreMountains.NiceVibrations;
using Unity.Mathematics;
using UnityEngine.Rendering.UI;
using UnityEngine.UI;
using UnityEngine.XR;


public class PlayerHandler : MonoBehaviour
{

   [Header("STATE")] 
   public bool isIdle;
   public bool isRunning;
   public bool isStacking;
   public bool isDead;
   public bool canMove; 
   public int cash;
   public int revenueDoubler = 1;

   [Space] [Header("MOVEMENT")]
   public float moveSpeed;
   public float currentMoveSpeed;
   private int moveSpeedMultiplayer;
   private CharacterController controller;
   private Vector3 movement;
   private float mSpeedY;
   private float mDesiredRotation = 0f;
   float y = 0;

   [Space] [Header("Box Stacking")] 
   public List<GameObject> stackedBoxes = new List<GameObject>();
   [SerializeField] List<Transform> stackPoints = new List<Transform>();
   public int maximumStackCount;
   
   [Header("Waypoint Target")] 
   [SerializeField] Transform wayPointTransform;
   [SerializeField] Transform wayPointTargetTransform;
   private bool canWayPoint;

   [Header("UI")] 
   public Transform UIPoint;
   public GameObject UIObject;
   public GameObject MaxStackObject;

   [Space] [Header("COMPONENT")] 
   public GameObject walkingVfx;
   public static PlayerHandler Instance;
   private FloatingJoystick joystick;
   private Animator anim;

   private void Awake()
   {
    //  Instance = this;
      InitComps();
   }

   private void Start()
   {
    //  GetPlayerPrefs();
     InitLate();
   }

   void GetPlayerPrefs()
   {
    
      if (PlayerPrefs.HasKey("CASH"))
      {
         int _cash = PlayerPrefs.GetInt("CASH");
         AddCash(_cash);
      }
      else if(!PlayerPrefs.HasKey("CASH"))
      {
       
      }
      
      
   }
   
   void InitComps()
   {
    
      Instance = this;
      controller = GetComponent<CharacterController>();
      joystick = GameObject.FindGameObjectWithTag("JOYSTICK").GetComponent<FloatingJoystick>();
      anim = GetComponent<Animator>();
      y = transform.position.y;
     
   }

   void InitLate()
   {
      currentMoveSpeed = moveSpeed;
      GameManager.Instance.SetCashText(cash);
      GameManager.Instance.SetBoxText(stackedBoxes.Count);
   }
   void StartMovement()
   {
      canMove = true;
   }
   private void Update()
   {
      if (canMove && !isDead && controller.enabled)
      {
         Movement();
         
      }
   }

   private void LateUpdate()
   {
      HandleWaypointData();
      HandleUI();
   }

   private void HandleUI()
   {
      Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
      UIObject.transform.position = UIPos;
   }


  public void SetWaypoint(Transform _target,bool _isActive)
   {
      wayPointTransform.gameObject.SetActive(_isActive);
      MMVibrationManager.Haptic(HapticTypes.MediumImpact);
      wayPointTargetTransform = _target;
      canWayPoint = _isActive;
      if (!_isActive)
      {
         GameManager.Instance.SetTutorialText("", false);
      }
   }

   void HandleWaypointData()
   {
      if (wayPointTargetTransform != null)
      {
         if (canWayPoint) wayPointTransform.LookAt(wayPointTargetTransform);

         float distance = Vector3.Distance(wayPointTargetTransform.position, transform.position);

         if (distance < 3f && canWayPoint)
         {
            SetWaypoint(null,false);
            GameManager.Instance.SetTutorialText("",false);
         }
        
      }
   }
   
   void Movement()
   {
      float x = joystick.Horizontal;
      float z = joystick.Vertical;
      
      Vector3 verticalMovement = Vector3.up * mSpeedY;
      movement = new Vector3(x,0,z).normalized;
      Vector3 rotatedMovement = Quaternion.Euler(0, Camera.main.transform.localRotation.eulerAngles.y, 0) * movement;
      controller.Move((verticalMovement + rotatedMovement) * currentMoveSpeed * Time.deltaTime);

      if (transform.position.y > 0.75)
      {
         y -= 1 * Time.deltaTime;
      }
      if (y < 0)
      {
         y = 0;
      }

         transform.position = new Vector3(transform.position.x,y,transform.position.z);
      
      if (rotatedMovement.magnitude > 0 )
      {
         mDesiredRotation = Mathf.Atan2(rotatedMovement.x, rotatedMovement.z) * Mathf.Rad2Deg;
        
         Quaternion currentRotation = transform.rotation;
         Quaternion targetRotation = Quaternion.Euler(0,mDesiredRotation,0);
         // Quaternion targetRotation = Quaternion.Euler(0,Camera.main.transform.localRotation.eulerAngles.y,0);
         transform.rotation = Quaternion.Lerp(currentRotation,targetRotation, 10 * Time.deltaTime);
      }
      if (joystick.Horizontal != 0 || joystick.Vertical != 0)
      {  
         anim.SetBool("Movement",true);
         isIdle = false;
         isRunning = true;
         if (Time.frameCount % 15 == 0)
         {
            GameObject walkfx = Instantiate(walkingVfx, transform.position, transform.rotation);
         }
      }
      else
      {
         anim.SetBool("Movement",false);
         isRunning = false;
         isIdle = true;
      }
   }
   
  
   public void AddCash(int _amount)
   {
      cash += _amount * revenueDoubler;
      CameraShaker.Instance.ShakeOnce(0.7f, 0.6f, 0.2f, 0.3f);
      MMVibrationManager.Haptic(HapticTypes.SoftImpact);
      if (cash >= 30 && GameManager.Instance.isFirstPlay)
      {
         GameManager.Instance.SetTutorialText("Build a Vending Machine",true);
         SetWaypoint(GameManager.Instance.tutorialTransforms[1],true);
      }
       GameManager.Instance.SetCashText(cash);
     // PrefsHandler.Instance.SetCash(cash);
     CheckVendingAvailability();
   }

   void CheckVendingAvailability()
   {
      if (!GameManager.Instance.isFirstPlay)
      {
         if (cash > 5 && cash < 10)
         {
            LevelItemsHandler.Instance.AddVendingMachineToScene();
         }

         if (cash > 10 && cash < 20)
         {
            LevelItemsHandler.Instance.AddVendingMachineToScene();
         }
         
         if(cash >20 && cash < 40)
         {
            LevelItemsHandler.Instance.AddVendingMachineToScene();
         }
      }
      
      
   }

   public void RemoveCash(int _amount)
   {
      cash -= _amount;
      CameraShaker.Instance.ShakeOnce(0.5f, 0.6f, 0.1f, 0.1f);
      MMVibrationManager.Haptic(HapticTypes.SoftImpact);
      if (cash < 0) cash = 0;
      GameManager.Instance.SetCashText(cash);
   }

   public void StackABox(GameObject _box)
   {
     _box.transform.SetParent(stackPoints[stackedBoxes.Count]);
      stackedBoxes.Add(_box);
      _box.transform.DOMove(stackPoints[stackedBoxes.Count - 1].position, 0.1f);
      StartCoroutine(FixBoxRot(_box.transform,stackPoints[stackedBoxes.Count-1]));
      MMVibrationManager.Haptic(HapticTypes.SoftImpact);
      anim.SetBool("StackedMovement",true);
      if (stackedBoxes.Count > 2 && GameManager.Instance.isFirstPlay)
      {
         SetWaypoint(GameManager.Instance.tutorialTransforms[1],true);
         GameManager.Instance.SetTutorialText("Load Goods in the Vending Machine",true);
      }

      if (stackedBoxes.Count == maximumStackCount)
      {
      
         MaxStackObject.SetActive(true);
      }
      else  if (stackedBoxes.Count < maximumStackCount)
      {
         MaxStackObject.SetActive(false);
      }
      GameManager.Instance.SetBoxText(stackedBoxes.Count);
   }

   public void ClearStack()
   {
      for (int i = 0; i < stackedBoxes.Count; i++)
      {
         Destroy(stackedBoxes[i]);
      }
      
      stackedBoxes.Clear();
      anim.SetBool("StackedMovement",false);
      GameManager.Instance.SetBoxText(0);
      
      if (stackedBoxes.Count < maximumStackCount)
      {
         MaxStackObject.SetActive(false);
      }
   }

   IEnumerator FixBoxRot(Transform _box,Transform _point)
   {
      yield return new WaitForSeconds(0.15f);
     
      _box.transform.localRotation = Quaternion.Euler(0,0,0);
      _box.transform.localPosition = new Vector3(0,0,0);
   }

   public void SetPlayerForLevel()
   {
      ClearStack();
      RemoveCash(9999);
      controller.enabled = false;
      transform.position = new Vector3(-2.9f,0.01f,-8.9f);
      controller.enabled = true;
      revenueDoubler = 1;
      maximumStackCount = GameManager.Instance.Level * 3;
      maximumStackCount = Mathf.Clamp(maximumStackCount, 3, 12);
   }

   public void RemoveABox()
   {
      
   }



}
