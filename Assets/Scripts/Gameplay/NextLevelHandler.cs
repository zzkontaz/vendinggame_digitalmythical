using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;
using UnityEngine;

public class NextLevelHandler : MonoBehaviour
{
    [SerializeField] private GameObject UIObject;
    [SerializeField] private Transform UIPoint;
    [SerializeField] private TextMeshProUGUI MoneyText;
    private bool isPoppedUp = false;
    [SerializeField] private  float  playerDistanceToCashFlow ,playerDistanceToPopUp;
    private bool isBought;
    private Transform player;
    [SerializeField] int moneyNeeded;
    private float cashRate;
    private float nextMoney;
    private float nextHaptic;

    private void Awake()
    {
      
        DOTween.Init();
    }

    private void Start()
    {
        Init();
    }

    void Init()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        SetMoneyText(moneyNeeded);

    }
    void HandleUI()
    {
        Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
        UIObject.transform.position = UIPos;
        
        if (Time.frameCount % 3 == 0)
        {
            UIObject.SetActive(!GameManager.Instance.isSecondCameraActive);
        }
    }

  
  

    private void LateUpdate()
    {
        HandleUI();
        HandlePlayerDistance();
    }
    
    void HandlePlayerDistance()
    {
        if (Time.frameCount % 1 == 0)
        {
            float distance = Vector3.Distance(player.position, transform.position);
         
            if (distance < 10)
            {
                HandleDistancePopUp(distance);
                HandleDistanceCashFlow(distance);
            }
        }
    }
    
    void HandleDistancePopUp(float distance)
    {
        if (distance < playerDistanceToPopUp && !isPoppedUp)
        {
            isPoppedUp = true;
            UIObject.transform.DOScale(new Vector3(1, 1, 1), 0.25f);
        }
        else if(distance >= playerDistanceToPopUp && isPoppedUp)
        {
            isPoppedUp = false;
            UIObject.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.25f);
        }
    }
    
    void HandleDistanceCashFlow(float distance)
    {
        if (distance < playerDistanceToCashFlow && moneyNeeded <= PlayerHandler.Instance.cash && moneyNeeded>0 && !isBought)
        {
            if (Time.time > nextMoney)
            {
                nextMoney = cashRate + Time.time;
            
                cashRate = cashRate / 2;
                cashRate = Mathf.Clamp(cashRate, 0.01f, 0.1f);
                moneyNeeded--;
                PlayerHandler.Instance.RemoveCash(1);
                MMVibrationManager.Haptic(HapticTypes.SoftImpact);
                GameObject _cash = Instantiate(PrefabsManager.Instance.CashGivenPrefab, transform.position,
                    Quaternion.identity);
                _cash.GetComponent<Cash>().GoToTarget(this.transform,cashRate * 2f);
                SetMoneyText(moneyNeeded);
               
                if (moneyNeeded <= 0 && !isBought)
                {
                    isBought = true;
                    Invoke("SetNextLevel",1);
                    
                }
            }
        }
    }

    void SetNextLevel()
    {
        GameManager.Instance.LevelUp();
    }

    void SetMoneyText(int _value)
    {
        MoneyText.text = _value.ToString();
    }
}
