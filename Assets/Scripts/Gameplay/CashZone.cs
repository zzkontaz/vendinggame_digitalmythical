using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CashZone : MonoBehaviour
{
   public int cashZoneID;
   [SerializeField] private int cashOrder;
   [SerializeField] private List<GameObject> cashList = new List<GameObject>();
   [SerializeField] private  List<Transform> cashPoints = new List<Transform>();
   private Transform playerTransform;
   private bool isPlayerNearby;
   private bool isPlayerCollecting;
   private float nextMoney;
   [SerializeField] private bool isTutorialMoney;

   private void Awake()
   {
      DOTween.Init();
   }

   private void Start()
   {
      playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
      InitCashPoints();
   }

   void InitCashPoints()
   {
      foreach (Transform points in transform.GetChild(0).transform)
      {
         cashPoints.Add(points);
      }
   }

   public void AddCash(int _amount)
   {
      for (int i = 0; i < _amount; i++)
      {
         GameObject _cashPrefab = PrefabsManager.Instance.CashPrefab;
         GameObject _cash = Instantiate(_cashPrefab, cashPoints[cashOrder].position, Quaternion.identity);
         cashOrder++;
         if (cashOrder >= cashPoints.Count - 1) cashOrder = 0;
         cashList.Add(_cash);
      }
   
   }

   private void LateUpdate()
   {
      HandlePlayerDistance();
   }

   public void HandlePlayerDistance()
   {
      if (Time.frameCount % 5 == 0)
      {
         float distance = Vector3.Distance(playerTransform.position, transform.position);

         if (distance >4 && distance < 5)
         {
            if (!isPlayerNearby)
            {
               isPlayerNearby = true;
             //  StartCoroutine(ShakeCash());

            }
         }
         else if(distance>=5)
         {
            if (isPlayerNearby) isPlayerNearby = false;
         }

         if (distance <= 2.75f && cashList.Count>0)
         {
            HandleMoneyToPlayer();
         }
         
      }
      
   }

   IEnumerator ShakeCash()
   {
      WaitForSeconds wait = new WaitForSeconds(0.1f);
      for (int i = cashList.Count-1; i >0; i--)
      {
         cashList[i].transform.DOShakeScale(0.5f, 1, 1, 1);
         yield return wait;
      }
   }

   void HandleMoneyToPlayer()
   {
      if (Time.time > nextMoney)
      {
         nextMoney = Time.time + 0.01f;
         Vector3 truePlayer = new Vector3(playerTransform.position.x,playerTransform.position.y + 1, playerTransform.position.z);
         if (cashList[cashList.Count - 1].gameObject != null)
         {
            cashList[cashList.Count-1].gameObject.GetComponent<CashCollectable>().transform.DOMove(truePlayer, 0.09f);
            cashList.Remove(cashList[cashList.Count-1]);
            cashOrder--;
         }

         if (cashList.Count == 0 && isTutorialMoney)
         {
            Destroy(gameObject);
            GameObject puff = Instantiate(PrefabsManager.Instance.VendingSpawnVFX, transform.position,
               Quaternion.identity);
         }
       
        // cashOrder--;
      }
   }
   
}
