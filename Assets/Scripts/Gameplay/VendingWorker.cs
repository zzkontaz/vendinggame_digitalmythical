using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

public class VendingWorker : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] Transform target;
    [SerializeField] Transform exit;
    [SerializeField] bool isArrived;
    [SerializeField] bool isFixed;
    [SerializeField] private GameObject fixingObject;
    [SerializeField] private GameObject emoji;
    [SerializeField] private GameObject goingFixingObject;
    [SerializeField] private GameObject UIObject;
    [SerializeField] private Transform UIPoint;
    private VendingMachine machine;
    private float distance;
    private Animator anim;
    private NavMeshAgent agent;

   
    
    void Awake()
    {
        DOTween.Init();
    }

    private void Start()
    {
        InitComps();
    }

    void InitComps()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        emoji.SetActive(false);
        fixingObject.SetActive(false);
    }
    
    public void SetWorker(VendingMachine _machine,Transform _exit)
    {
        machine = _machine;
        target = _machine.gameObject.transform;
        exit = _exit;
    }

    void SetFixing()
    {
        if (!isFixed)
        {
            isFixed = true;
            fixingObject.SetActive(true);
            emoji.SetActive(false);
            goingFixingObject.SetActive(false);
            transform.LookAt(target);
            StartCoroutine(FixMachine());
        }
      
    }

    IEnumerator FixMachine()
    {
        yield return new WaitForSeconds(2f);
        machine.VendingMachineFixed();
        anim.SetBool("Movement",true);
        agent.speed = moveSpeed;
        isArrived = false;
        agent.SetDestination(exit.position);
        target = exit;
        TamirZone.Instance.WorkerFixedTheProblem();
        Destroy(gameObject,10);
    }

    private void LateUpdate()
    {
        HandleDistance();
        HandleUI();
    }

    void HandleUI()
    {
        Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
        UIObject.transform.position = UIPos;
        
        if (Time.frameCount % 3 == 0)
        {
            UIObject.SetActive(!GameManager.Instance.isSecondCameraActive);
        }
    }
    void HandleDistance()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance >= 1.5f)
        {
            if (Time.frameCount % 15 == 0)
            {
                agent.SetDestination(target.position);
            }

            isArrived = false;
            anim.SetBool("Movement",true);
            
            agent.speed = moveSpeed;
        }
        else if (distance < 1.5f && !isArrived)
        {
            agent.speed = 0;
            agent.velocity = Vector3.zero;
            anim.SetBool("Movement",false);
            isArrived = true;
            SetFixing();
        }
    }
        
    
}
