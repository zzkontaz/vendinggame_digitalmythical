using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using EZCameraShake;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.Rendering.UI;

public class CandyFillingHandler : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;
    [SerializeField] private List<GameObject> items = new List<GameObject>();
    [SerializeField] private List<Transform> itemPoints = new List<Transform>();
    [SerializeField] private List<GameObject> oldItems = new List<GameObject>();
    [SerializeField] private bool canAddItems;
    [SerializeField] private Camera vendingCamera;
    [SerializeField] private GameObject tutorialObject;
    private int itemCount;
    private VendingMachine vendingMachine;
    public static CandyFillingHandler Instance;
    private float nextTouch;
    
    
    private void Awake()
    {
        Instance = this;
        DOTween.Init();
    }
    
    
    public void StartItemFilling(int _ItemID,int _currentItemCount,int _machineID)
    {
        for (int i = 0; i < items.Count; i++)
        {
            Destroy(items[i]);
        }
        
        

        itemCount = PlayerHandler.Instance.stackedBoxes.Count * 10;
        GameManager.Instance.SetVendingSlider(_ItemID,_currentItemCount,120);
        GameManager.Instance.SwitchCandyCam();
        CheckItems(_currentItemCount);
        if (GameManager.Instance.isFirstPlay)
        {
            tutorialObject.SetActive(true);
        }

      
        for (int i = 0; i < GameManager.Instance.vendingMachines.Count; i++)
        {
            if (GameManager.Instance.vendingMachines[i].vendingMachineID == _machineID)
            {
                vendingMachine = GameManager.Instance.vendingMachines[i];
            }
        }

        canAddItems = true;
    }
    
    void CheckItems(int _currentItemCount)
    {
        
        GameObject _itemPrefab =PrefabsManager.Instance.CandyPrefabs[Random.Range(0, PrefabsManager.Instance.CandyPrefabs.Length)];
        for (int i = 0; i < _currentItemCount ; i++)
        {
            GameObject _item = Instantiate(_itemPrefab, itemPoints[i].position,Quaternion.identity,itemPoints[i]);
            oldItems.Add(_item);
        }
    }
    
    public void AddItemToHere(Vector3 _point)
    {
        AddItemToVendingMachine();
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
            CameraShaker.Instance.ShakeOnce(0.15f, 0.25f, 0.3f, 0.3f);
            GameObject _candyPrefab =
                PrefabsManager.Instance.CandyPrefabs[Random.Range(0, PrefabsManager.Instance.CandyPrefabs.Length)];
            GameObject _candy = Instantiate(_candyPrefab, _point, Quaternion.identity);
            _candy.transform.localRotation  = Quaternion.Euler(0,0,Random.Range(-20,30));
           // _candy.GetComponent<Rigidbody>().AddForce(transform.up * 2, ForceMode.Impulse);

           items.Add(_candy);
           itemCount--;
            tutorialObject.SetActive(false);
            GameManager.Instance.IncreaseVendingSlider();
            if (itemCount <= 0)
            {
                itemCount = 0;
                ResetVendingTimer();
            }
        }
    
     void ResetVendingTimer()
       {
          canAddItems = false;
          PlayerHandler.Instance.ClearStack();
          Invoke("SetFinishEffect",0.5f);
          vendingMachine.ItemsLoadFinish();
          // Todo: SATISFACTION KONULACAK
          StartCoroutine(ResetVending(2.25f));
       }
    
       void SetFinishEffect()
       {
          GameManager.Instance.SetVendingFinishText();
     
       }
    
       IEnumerator ResetVending(float timer)
       {
        yield return new WaitForSeconds(timer);
          for (int i = 0; i < items.Count; i++)
          {
             Destroy(items[i]);
          }

          for (int i = 0; i < oldItems.Count; i++)
          {
             Destroy(oldItems[i]);
          }
          items.Clear();
          oldItems.Clear();
          canAddItems = false;
          GameManager.Instance.SwitchGameCam();
         
       }
    
       void HandleTouchInput()
       {
          ray = vendingCamera.ScreenPointToRay(Input.mousePosition);
          if(Physics.Raycast(ray, out hit))
          {
             if (Input.GetMouseButton(0) && hit.collider.gameObject.CompareTag("ITEMPOINT") && canAddItems & Time.time> nextTouch)
             {
                 nextTouch = Time.time + 0.1f;
                 MMVibrationManager.Haptic(HapticTypes.LightImpact);
                 // Vector3 worldPosition = new Vector3(Input.mousePosition.x,Input.mousePosition.y,hit.collider.transform.position.z);
                 Vector3 screenPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 9.5f);
                 Vector3 worldPos = vendingCamera.ScreenToWorldPoint(screenPos);
                 Debug.Log(Input.mousePosition);
              //   worldPosition.z = hit.collider.transform.position.z;
                 AddItemToHere(worldPos);
             }
                  
          }
       }
    
       void AddItemToVendingMachine()
       {
          vendingMachine.AddItemToVendingMachine();
       }

       public void RemoveItemInMachine()
       {
           vendingMachine.RemoveItem();
       }
    
       void Update()
       {
           HandleTouchInput();
       }
}
