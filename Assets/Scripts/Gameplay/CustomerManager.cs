using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerManager : MonoBehaviour
{
    public int maxCustomers,currentCustomers;
    private Transform customerSpawnPoint,customerExitPoint;
    private Transform vendingMachinePoint;
    public float customerSpawnRate;
    private float nextCustomer;
    private int totalCustomersCount;
    public bool canCustomerSpawn;
    private VendingMachine machine;
    private bool isMachineBroken;
    public List<Customer> Customers = new List<Customer>();
    
    private void Start()
    {
        GetTransforms();
    }

    public void InitVendingMachine(VendingMachine _machine)
    {
        machine = _machine;
    }
    void GetTransforms()
    {
        customerSpawnPoint = GameObject.FindGameObjectWithTag("CUSTOMERSPAWN").transform;
        customerExitPoint = GameObject.FindGameObjectWithTag("CUSTOMEREXIT").transform;
    }

   public void SetVendingPoint(Transform _vendingPoint)
    {
        vendingMachinePoint = _vendingPoint;
    }

    private void LateUpdate()
    {
        HandleCustomerSpawn();
    }
    
    

    void HandleCustomerSpawn()
    {
        if (Time.frameCount % 5 == 0)
        {
            if (Time.time > nextCustomer && currentCustomers < maxCustomers && canCustomerSpawn && !isMachineBroken)
            {
                nextCustomer = customerSpawnRate + Time.time;
                CreateACustomer();
                
            }
        }
    }
    
    public void CreateACustomer()
    {
        // Stil ayarı
        int hairRD = UnityEngine.Random.Range(0, GameManager.Instance.hairs.Count);
        int tex = UnityEngine.Random.Range(0, GameManager.Instance.textures.Count);
        GameObject hairSelect = GameManager.Instance.hairs[hairRD].gameObject;
        
        GameObject _customerPrefab = PrefabsManager.Instance.CustomerPrefab;
           GameObject _customer = Instantiate(_customerPrefab, customerSpawnPoint.position, Quaternion.identity);
        _customer.GetComponent<Customer>().SetCustomerDesign(hairSelect,GameManager.Instance.textures[tex]);
        _customer.GetComponent<Customer>().SetCustomerManager(this);
        _customer.GetComponent<Customer>().InitCustomer(vendingMachinePoint,customerExitPoint,machine);
        if (Customers.Count == 0)
        {
            _customer.GetComponent<Customer>().SetCustomerTransform(vendingMachinePoint,true);
        }
        else
        {
            _customer.GetComponent<Customer>().SetCustomerTransform(Customers[Customers.Count-1].gameObject.transform,false);
        }
        currentCustomers++;
        totalCustomersCount++;
        Customers.Add(_customer.GetComponent<Customer>());

        GameManager.Instance.isFirstPlay = false;
    }
    
    // TODO: Customer sırasına göz atılacak;
    
    public void RemoveCustomer(Customer _customer)
    {
        Customers.Remove(_customer);
        currentCustomers--;
       
        if (Customers.Count > 0)
        {
            Customers[0].SetCustomerTransform(vendingMachinePoint,true);
        }
        StartCoroutine(SetCustomersOrder());
    }

    IEnumerator SetCustomersOrder()
    {    
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        if(Customers.Count>1)
            for (int i = 1; i < Customers.Count; i++)
            {
                Customers[i].SetCustomerTransform(Customers[i-1].transform,false);
                yield return wait;
            }
    }
    
    public void SetMachineBroke(bool _value)
    {
        isMachineBroken = _value;
        for (int i = 0; i < Customers.Count; i++)
        {
            Customers[i].SetMachineBroke(_value);
        }
    }

    
}
