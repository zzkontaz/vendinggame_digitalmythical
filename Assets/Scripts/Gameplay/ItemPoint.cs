using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPoint : MonoBehaviour
{
   public int ID;

   public void SelectZone()
   {
      VendingFillingHandler.Instance.AddItemToHere(ID);
   }
}

