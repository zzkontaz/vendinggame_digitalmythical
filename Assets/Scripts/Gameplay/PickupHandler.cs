using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class PickupHandler : MonoBehaviour
{
   public bool canPickupSpawn;
   [SerializeField] private List<Transform> pickupSpawnPoints;
   [SerializeField] private float pickupRate;
   private float nextPickup = 60;
   private int pickupOnScene;
   public GameObject pickupObject;
   public static PickupHandler Instance;

   private void Awake()
   {
      Instance = this;
   }

   private void LateUpdate()
   {
      HandlePickupSpawn();
   }

   void HandlePickupSpawn()
   {
      if (Time.frameCount % 5 == 0)
      {
         if (Time.time > nextPickup && canPickupSpawn)
         {
            nextPickup = Time.time + pickupRate;
            SetNewPickupRate();
            SelectPickup();
         }
      }
   }

   void SetNewPickupRate()
   {
      pickupRate = UnityEngine.Random.Range(60, 90);
   }
   void SelectPickup()
   {
      int selection = 0;
      if (PlayerHandler.Instance.cash > 40)
      {
         selection = 0;
      }
         if (PlayerHandler.Instance.cash > 25)
         {
            selection = 1;
         }

         if (GameManager.Instance.brokenVendingMachines.Count > 0)
         {
            selection = 2;
         }

         if (PlayerHandler.Instance.cash < 25)
         {
            selection = 3;
         }
         SpawnPickup(selection);
   }

   void SpawnPickup(int id)
   {
      if(pickupObject !=null) Destroy(pickupObject);
         
      
      GameObject _pickupPrefab = PrefabsManager.Instance.pickupPrefabs[id];
      GameObject _pickup = Instantiate(_pickupPrefab,
         pickupSpawnPoints[UnityEngine.Random.Range(0, pickupSpawnPoints.Count)].position, Quaternion.identity);

      pickupObject = _pickup.gameObject;
   }

   public void AcceptPickup()
   {
      int ID = pickupObject.GetComponent<Pickup>().ID;
      switch (ID)
      {
         case 0 :
            PlayerHandler.Instance.maximumStackCount = 12;
            break;
         case 1 :
            PlayerHandler.Instance.revenueDoubler = 2;
            break;
         case 2 :
            for (int i = 0; i < GameManager.Instance.brokenVendingMachines.Count; i++)
            {
               GameManager.Instance.brokenVendingMachines[i].VendingMachineFixed();
            }
            GameManager.Instance.brokenVendingMachines.Clear();
            break;
         case 3 :
            PlayerHandler.Instance.AddCash(50);
            break;
      }
      RemovePickup();
   }

   public void RemovePickup()
   {
      Destroy(pickupObject);
      pickupObject = null;
   }
}
