using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = System.Random;

public class Cash : MonoBehaviour
{
  private Rigidbody rb;
  private void Awake()
  {
    DOTween.Init();
    rb = GetComponent<Rigidbody>();

  }

  private void Start()
  {
    Transform player = GameObject.FindGameObjectWithTag("Player").transform;
    transform.position = new Vector3(player.position.x,player.position.y + 1f,player.position.z + 0.5f);
  
  }

  public void GoToTarget(Transform _target,float _duration)
  {
    float forceRD = UnityEngine.Random.Range(6, 8);
    int x =  UnityEngine.Random.Range(-50, 50);
    int y =  UnityEngine.Random.Range(-50, 50);
    int z =  UnityEngine.Random.Range(-50, 50);
    transform.rotation = Quaternion.Euler(x,y,z);
    rb.AddForce(Vector3.up * forceRD,ForceMode.Impulse);
    StartCoroutine(GoToTargetTimer(_target, _duration));
    

  }

  IEnumerator GoToTargetTimer(Transform _target, float _duration)
  {
    yield return new WaitForSeconds(0.5f);
    rb.useGravity = false;
    Vector3 truePos = new Vector3(_target.position.x, _target.position.y + 1, _target.position.z);
    gameObject.transform.DOMove(truePos, _duration);
    Destroy(gameObject,_duration+0.05f);
  }
}
