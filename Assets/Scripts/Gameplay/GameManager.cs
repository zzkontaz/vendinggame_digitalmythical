using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Rendering;
using UnityEngine.Rendering.UI;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public  class GameManager: MonoBehaviour
{
    public static GameManager Instance;
    public int Level;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI cashText;
    public TextMeshProUGUI boxText;
    public bool isSecondCameraActive;
    [Header("Vending Machine Data")]
    public int[] vendingMachineBuildCost;
    public Material[] vendingMachineMaterials;
    public List<VendingMachine> vendingMachines = new List<VendingMachine>();
    public Sprite[] vendingMachineItemSprites;
    public List<string> vendingFillingStrings = new List<string>();
    public TextMeshProUGUI vendingFinishText;
    public List<Color> vendingColors = new List<Color>();
    public Slider vendingFillingSlider;
    public Image vendingSliderImage;
    public GameObject vendingFinishAnim;
    public List<Sprite> emojis = new List<Sprite>();
    public List<VendingMachine> brokenVendingMachines = new List<VendingMachine>();
    public GameObject vendingUpgradeButtonObject;
    
    [Header("Pickups")]
    public List<String> pickupNames;
    public TextMeshProUGUI pickupNameText;
    
    [Header("Tutorial")] 
    [SerializeField]  public bool isFirstPlay;
    [SerializeField]  public TextMeshProUGUI tutorialText;
    public  Transform[] tutorialTransforms;
    
    [Header("Customer")]
    public List<GameObject> hairs = new List<GameObject>();
    public List<Texture2D> textures = new List<Texture2D>();
    public Transform customerSpawnPoint,customerExitPoint;
    
    [Header("Camera Controller")] 
    public GameObject[] cameras;
    public Vector3[] povCamPoints;
    public VolumeComponent dof;
    public GameObject joystickCanvas;

    [Header("Level Data")] [SerializeField]
    private Transform LevelSpawnPos;
    [SerializeField] private List<GameObject> levelPrefabs;
    [SerializeField] private Transform currentLevel;

    [Header("Intersitital Reklam")] 
    [SerializeField] private float currentAdTimer;
    private float nextAd = 100;
    private int treeZoneCurrentAd;
    
    
    public GameObject GameUI;
    public GameObject PauseUI;
    public GameObject VendingUI;
    public GameObject PauseView;
    public GameObject PickupPopUp;
    public GameObject LevelPassingCover;

    void Awake()
    {
        Instance = this;
        DOTween.Init();
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
    }

    
    private void Start()
    {
       
        Volume volume = FindObjectOfType<Volume>();
       DepthOfField tmp;
       if (volume.profile.TryGet<DepthOfField>(out tmp))
       {
           dof = tmp;
       }
       
       if (PlayerPrefs.HasKey("LEVEL"))
       {
           Level = PlayerPrefs.GetInt("LEVEL");
       }
       else
       {
           Level = 1;
           SetLevelText(Level);

       }
       SetLevel();
    }

    public void LevelUp()
    {
        Level++;
        isFirstPlay = false;
        SetLevel();
    }
    public void SetLevel()
    {    
        if (currentLevel != null)
        {
            Destroy(currentLevel.transform.gameObject);
        }
        
       CheckOldObjects();
        LevelPassingCover.SetActive(true);
        GameObject _level = Instantiate(levelPrefabs[Level], LevelSpawnPos.position, Quaternion.identity);
        currentLevel = _level.transform;
        GetTransforms();
        SwitchGameCam();
        SetLevelText(Level);
        CheckFirstPlay();
       Invoke("SetPlayerForLevel",0.6f);
        Invoke("SetPassingCoverOff",0.75f);
    }

    void CheckOldObjects()
    {
        if (GameObject.FindGameObjectWithTag("CUSTOMER") != null)
        {
            GameObject[] _customers = GameObject.FindGameObjectsWithTag("CUSTOMER");
            for (int i = 0; i <_customers.Length; i++)
            {
                Destroy(_customers[i]);
            }
        }
        if (GameObject.FindGameObjectWithTag("CASHGIVEN") != null)
        {
            GameObject[] _cashGivens = GameObject.FindGameObjectsWithTag("CASHGIVEN");
            for (int i = 0; i <_cashGivens.Length; i++)
            {
                Destroy(_cashGivens[i]);
            }
        }
        if (GameObject.FindGameObjectWithTag("BOX") != null)
        {
            GameObject[] _box = GameObject.FindGameObjectsWithTag("BOX");
            for (int i = 0; i <_box.Length; i++)
            {
                Destroy(_box[i]);
            }
        }
        if (GameObject.FindGameObjectWithTag("CUSTOMER") != null)
        {
            GameObject[] _customer = GameObject.FindGameObjectsWithTag("CUSTOMER");
            for (int i = 0; i <_customer.Length; i++)
            {
                Destroy(_customer[i]);
            }
        }
    }

    void SetPlayerForLevel()
    {
        PlayerHandler.Instance.SetPlayerForLevel();
    }

    void SetPassingCoverOff()
    {
        LevelPassingCover.SetActive(false);
    }

    void GetTransforms()
    {
        customerSpawnPoint = GameObject.FindGameObjectWithTag("CUSTOMERSPAWN").transform;
        customerExitPoint = GameObject.FindGameObjectWithTag("CUSTOMEREXIT").transform;
    }
    void CheckFirstPlay()
    {
       
       // isFirstPlay = PlayerPrefs.GetInt("FIRST_PLAY") == 0;
        if (isFirstPlay)
        {
            SetTutorialText("Get the money",true);
            tutorialTransforms[0] = GameObject.Find("CashZone").transform;
            tutorialTransforms[1] = GameObject.Find("TutorialVendingZone").transform;
            tutorialTransforms[2] = GameObject.Find("ShipmentZone").transform;
            PlayerHandler.Instance.SetWaypoint(tutorialTransforms[0],true);
        }
        else
        {
            PlayerHandler.Instance.SetWaypoint(null,false);
            SetTutorialText("",false);
        }
    }
    
    private void LateUpdate()
    {
        HandleIntersititalTimer();
    }

    public void SetTutorialText(string _text,bool _isActive)
    {
        tutorialText.transform.parent.gameObject.SetActive(_isActive);
        tutorialText.text = _text;
    }
    

    public void SetCashText(int amount)
    {
        cashText.text = amount.ToString();
        cashText.transform.parent.gameObject.transform.DOShakeScale(0.1f, 0.01f);
        Invoke("ResetTextScales",0.5f);
    }

    public void SetBoxText(int amount)
    {
        boxText.text = amount.ToString();
        boxText.transform.parent.transform.DOShakeScale(0.1f, 0.001f);
    }

    public void SetLevelText(int amount)
    {
        levelText.text = "Level " + amount.ToString();
    }

    public void ResetTextScales()
    {
        cashText.transform.parent.transform.localScale = new Vector3(1, 1, 1);
    }


    public void AddVendingMachine(VendingMachine _vendingMachine)
    {
        vendingMachines.Add(_vendingMachine);
    }

    public void SwitchGameCam()
    {
        cameras[0].SetActive(true);
        cameras[1].SetActive(false);
        cameras[2].SetActive(false);
        cameras[3].SetActive(false);
        joystickCanvas.SetActive(true);
        PlayerHandler.Instance.canMove = true;
        GameUI.SetActive(true);
        VendingUI.SetActive(false);
        isSecondCameraActive = false;
    }

    public void SwitchVendingCam()
    {
     //   cameras[0].SetActive(false);
        cameras[1].SetActive(true);
        cameras[2].SetActive(false);
        cameras[3].SetActive(false);
        joystickCanvas.SetActive(false);
        PlayerHandler.Instance.canMove = false;
        GameUI.SetActive(false);
        VendingUI.SetActive(true);
        isSecondCameraActive = true;
    }

    public void SwitchCandyCam()
    {
        cameras[1].SetActive(false);
        cameras[2].SetActive(true);
        cameras[3].SetActive(false);
        joystickCanvas.SetActive(false);
        PlayerHandler.Instance.canMove = false;
        GameUI.SetActive(false);
        VendingUI.SetActive(true);
        isSecondCameraActive = true;
    }

    public void SwitchToyCam()
    {
        cameras[1].SetActive(false);
        cameras[2].SetActive(false);
        cameras[3].SetActive(true);
        joystickCanvas.SetActive(false);
        PlayerHandler.Instance.canMove = false;
        GameUI.SetActive(false);
        VendingUI.SetActive(true);
        isSecondCameraActive = true;
    }
    
    void HandleIntersititalTimer()
    {
        if (Time.frameCount % 30 == 0)
        {
            if (Time.time > nextAd)
            {
                nextAd = Time.time + currentAdTimer;
                WatchIntersitial();
            }
        }
       
    }

    void WatchIntersitial()
    {
        // Todo: Intersitital
    }

    public void SetVendingSlider(int ID,int cur,int max)
    {
        vendingFillingSlider.maxValue = max;
        vendingFillingSlider.value = cur;
        vendingFillingSlider.fillRect.gameObject.GetComponent<Image>().color = vendingColors[ID];
        vendingSliderImage.sprite = vendingMachineItemSprites[ID];
        vendingSliderImage.transform.DOShakeScale(0.15f, 1, 2);
     
    }

    public void SetVendingFinishText()
    {
       
        int textRD = UnityEngine.Random.Range(0, vendingFillingStrings.Count);
        vendingFinishText.text = vendingFillingStrings[textRD];
        vendingFinishText.gameObject.SetActive(true);
        vendingFinishAnim.SetActive(true);
        Invoke("ResetFinishText", 1.5f);

    }

    void ResetFinishText()
    {
        vendingFinishText.transform.gameObject.SetActive(false);
        vendingFinishAnim.SetActive(false);
    }

    public void IncreaseVendingSlider()
    {
        vendingFillingSlider.value = vendingFillingSlider.value + 1;
    }

    public void SetBrokenMachines(bool _isBroke, VendingMachine _machine)
    {
        if(_isBroke) brokenVendingMachines.Add(_machine);
        else if (!_isBroke)
        {
            _machine.VendingMachineFixed();
            brokenVendingMachines.Remove(_machine);
        }
        
    }
    public void PauseGame()
    {
        MMVibrationManager.Haptic(HapticTypes.SoftImpact);
        PauseView.SetActive(true);
        Time.timeScale = 0;
    }
    
    public void UnPauseGame()
    {
        Time.timeScale = 1;
        MMVibrationManager.Haptic(HapticTypes.SoftImpact);
        PauseView.SetActive(false);
    }

    public void SetPickupPopUp(int value,bool isOpen)
    {
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
        PickupPopUp.SetActive(isOpen);
        pickupNameText.text = pickupNames[value];
    }

    public void AcceptPickUp()
    {
        PickupHandler.Instance.AcceptPickup();
        SetPickupPopUp(0,false);
    }

    public void DeclinePickUp()
    {
        PickupHandler.Instance.RemovePickup();
        SetPickupPopUp(0,false);
    }

    public void SetVendingUpgradeButton(bool _isActive)
    {
       vendingUpgradeButtonObject.SetActive(_isActive);
    }
   
  
}


