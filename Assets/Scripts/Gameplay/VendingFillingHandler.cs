using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using EZCameraShake;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.Rendering.UI;


public class VendingFillingHandler : MonoBehaviour
{
   Ray ray;
   RaycastHit hit;
   [SerializeField] private MeshRenderer renderer;
   [SerializeField] private List<Transform> itemPoints = new List<Transform>();
   [SerializeField] private List<Transform> boxItemPoints = new List<Transform>();
   [SerializeField] private List<GameObject> items = new List<GameObject>();
   [SerializeField] private List<GameObject> oldItems = new List<GameObject>();
   [SerializeField] private List<bool> isFilled = new List<bool>();
   [SerializeField] private int boxOrder;
   [SerializeField] private bool canAddItems;
   [SerializeField] private Transform boxTransform;
   [SerializeField] private Transform boxSpawnPoint, boxPoint;
   [SerializeField] private Camera vendingCamera;
   [SerializeField] private Transform itemPointParent;
   [SerializeField] private GameObject tutorialObject;
   [SerializeField] private List<GameObject> lockZones;
   private int unlockedCount;
   private int totalItems;
   private VendingMachine vendingMachine;
   public static VendingFillingHandler Instance;

   private void Awake()
   {
      Instance = this;
      DOTween.Init();
   }

   private void Start()
   {
      InitItemPoints();
   
   }
   void InitItemPoints()
   {
      for (int i = 0; i < itemPointParent.transform.childCount; i++)
      {
         itemPointParent.transform.GetChild(i).GetComponent<ItemPoint>().ID = i;
      }
      tutorialObject.SetActive(false);
   }
   public void StartItemFilling(int _ItemID,int _currentItemCount,int _machineID)
   {
      for (int i = 0; i < items.Count; i++)
      {
         Destroy(items[i]);
      }

      for (int i = 0; i < isFilled.Count; i++)
      {
         isFilled[i] = false;
      }
      
      boxOrder = 0;
      renderer.material = GameManager.Instance.vendingMachineMaterials[_ItemID];
      GameManager.Instance.SetVendingSlider(_ItemID,_currentItemCount,24);
      CheckItems(_ItemID,_currentItemCount);
      GameManager.Instance.SwitchVendingCam();
      SetBox(_ItemID);
      if (GameManager.Instance.isFirstPlay)
      {
         tutorialObject.SetActive(true);
      }

      for (int i = 0; i < GameManager.Instance.vendingMachines.Count; i++)
      {
         if (GameManager.Instance.vendingMachines[i].vendingMachineID == _machineID)
         {
            vendingMachine = GameManager.Instance.vendingMachines[i];
            
         }
      }


      if (PlayerHandler.Instance.cash > 25 || PlayerHandler.Instance.stackedBoxes.Count > 3 || vendingMachine.unlockedParts < 3)
      {
         GameManager.Instance.SetVendingUpgradeButton(true);
      }
   }
   void CheckItems(int _ItemID,int _currentItemCount)
   {
      GameObject _itemPrefab = PrefabsManager.Instance.ItemPrefabs[_ItemID];
      for (int i = 0; i < _currentItemCount; i++)
      {
         GameObject _item = Instantiate(_itemPrefab, itemPoints[i].position,Quaternion.identity,itemPoints[i]);
         oldItems.Add(_item);
         isFilled[i] = true;
         totalItems++;
      }
      
    
   }
   void SetBox(int _ItemID)
   {
   
      int amount = PlayerHandler.Instance.stackedBoxes.Count * 2;
    
      boxTransform.position = boxSpawnPoint.position;
      
      GameObject _itemPrefab = PrefabsManager.Instance.ItemPrefabs[_ItemID];
      for (int i = 0; i < amount; i++)
      {
         GameObject _item = Instantiate(_itemPrefab, boxItemPoints[i].position,boxItemPoints[i].rotation,boxItemPoints[i]);
         items.Add(_item);
      }
      boxTransform.DOMove(boxPoint.position, 1f);
      canAddItems = true;
      
     
   }
   public void AddItemToHere(int _transformID)
   {
      if (!isFilled[_transformID] && items[boxOrder]!=null)
      {
         items[boxOrder].transform.SetParent(itemPoints[_transformID]);
         items[boxOrder].gameObject.transform.DOMove(itemPoints[_transformID].position, 0.5f);
         items[boxOrder].transform.DORotate(new Vector3(0, 0, 0), 0.25f);
         boxOrder++;
         totalItems++;
         AddItemToVendingMachine();
         MMVibrationManager.Haptic(HapticTypes.LightImpact);
         CameraShaker.Instance.ShakeOnce(0.35f, 0.25f, 0.3f, 0.3f);
         isFilled[_transformID] = true;
         boxTransform.DOLocalMoveX(boxTransform.localPosition.x - 0.135f , 0.25f);
         tutorialObject.SetActive(false);
         GameManager.Instance.IncreaseVendingSlider();
         if (boxOrder == PlayerHandler.Instance.stackedBoxes.Count *2 || totalItems  >= unlockedCount )
         {
            ResetVendingTimer();
         }
      }
   }
   void ResetVendingTimer()
   {
      canAddItems = false;
      totalItems = 0;
      PlayerHandler.Instance.ClearStack();
      Invoke("SetFinishEffect",0.5f);
      vendingMachine.ItemsLoadFinish();
      // Todo: SATISFACTION KONULACAK
      StartCoroutine(ResetVending(2.25f));
   }
   void SetFinishEffect()
   {
      GameManager.Instance.SetVendingFinishText();
 
   }
   IEnumerator ResetVending(float timer)
   {
    yield return new WaitForSeconds(timer);
      for (int i = 0; i < items.Count; i++)
      {
         Destroy(items[i]);
      }

      for (int i = 0; i < isFilled.Count; i++)
      {
         isFilled[i] = false;
      }

      for (int i = 0; i < oldItems.Count; i++)
      {
         Destroy(oldItems[i]);
      }
      items.Clear();
      oldItems.Clear();
      boxOrder = 0;
      canAddItems = false;
      GameManager.Instance.SwitchGameCam();
      GameManager.Instance.SetVendingUpgradeButton(false);
     
   }
   void HandleTouchInput()
   {
      ray = vendingCamera.ScreenPointToRay(Input.mousePosition);
      if(Physics.Raycast(ray, out hit))
      {
         if (Input.GetMouseButton(0) && hit.collider.gameObject.CompareTag("ITEMPOINT") && canAddItems)
         {
            hit.collider.gameObject.GetComponent<ItemPoint>().SelectZone();
         }
              
      }
   }
   void AddItemToVendingMachine()
   {
      vendingMachine.AddItemToVendingMachine();
   }

   public void UnlockZones(int _amount)
   {
      
      for (int i = 0; i < lockZones.Count; i++)
      {
         lockZones[i].SetActive(true);
         lockZones[i].GetComponent<Animator>().SetTrigger("Lock");
      }
      for (int i = 0; i < _amount; i++)
      {
         lockZones[i].SetActive(false);
      }

      unlockedCount = (_amount * 6) + 6;
   }

   public void UnlockNow()
   {
      if (PlayerHandler.Instance.cash >= 50)
      {
         for (int i = 0; i < lockZones.Count; i++)
         {
            if (lockZones[i].activeSelf)
            {
               vendingMachine.UnlockAZone();
               lockZones[i].GetComponent<Animator>().SetTrigger("Unlock");
               StartCoroutine(UnlockAZoneTimer(lockZones[i].gameObject));
               PlayerHandler.Instance.RemoveCash(50);
               MMVibrationManager.Haptic(HapticTypes.SoftImpact);
               break;
            }
         } 
      }
     
   }

   IEnumerator UnlockAZoneTimer(GameObject _object)
   {
      yield return new WaitForSeconds(0.5f);
      _object.SetActive(false);
   }
   void Update()
   {
     HandleTouchInput();
   }
   
   
}
