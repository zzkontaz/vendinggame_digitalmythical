using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class TamirZone : MonoBehaviour
{
   
    [SerializeField] private GameObject UIObject;
    [SerializeField] private Transform UIPoint;
    [SerializeField] private GameObject MainUIObject;
    [SerializeField] private GameObject WorkerCallObject;
    [SerializeField] private TextMeshProUGUI MoneyText;
    [SerializeField] private Transform WorkerSpawnPoint;
    [SerializeField] private GameObject wrenchObject;
    private bool isMachineBroke;
    private bool isPoppedUp = false;
    public float  playerDistanceToCashFlow ,playerDistanceToPopUp;
    private bool isBought;
    private Transform player;
    private int moneyNeeded;
    [SerializeField] int totalNeeded;
    private float cashRate;
    private float nextMoney;
    private float nextHaptic;
    public static TamirZone Instance;

    private void Awake()
    {
        Instance = this;
        DOTween.Init();
    }

    private void Start()
    {
        Init();
        ResetMoneyFlow();
    }

    void Init()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        WorkerSpawnPoint = GameObject.FindGameObjectWithTag("CUSTOMERSPAWN").transform;
        MainUIObject.SetActive(true);
        WorkerCallObject.SetActive(false);
        wrenchObject.SetActive(false);
    }
    void HandleUI()
    {
        Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
        UIObject.transform.position = UIPos;
        if (Time.frameCount % 3 == 0)
        {
            UIObject.SetActive(!GameManager.Instance.isSecondCameraActive);
        }
    }

  
  

    private void LateUpdate()
    {
        HandleUI();
        HandlePlayerDistance();
    }

    public void SetWrenchAnim(bool _value)
    {
        wrenchObject.SetActive(_value);
    }
    void HandlePlayerDistance()
    {
        if (Time.frameCount % 2 == 0)
        {
            float distance = Vector3.Distance(player.position, transform.position);
         
            if (distance < 10)
            {
                HandleDistancePopUp(distance);
                HandleDistanceCashFlow(distance);
            }
        }
    }
    
    void HandleDistancePopUp(float distance)
    {
        if (distance < playerDistanceToPopUp && !isPoppedUp)
        {
            isPoppedUp = true;
            UIObject.transform.DOScale(new Vector3(1, 1, 1), 0.25f);
        }
        else if(distance >= playerDistanceToPopUp && isPoppedUp)
        {
            isPoppedUp = false;
            UIObject.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.25f);
        }
    }
    
    void HandleDistanceCashFlow(float distance)
    {
        if (distance < playerDistanceToCashFlow && moneyNeeded <= PlayerHandler.Instance.cash && moneyNeeded>0 && GameManager.Instance.brokenVendingMachines.Count>0 && !isBought)
        {
            if (Time.time > nextMoney)
            {
                nextMoney = cashRate + Time.time;
            
                cashRate = cashRate / 2;
                cashRate = Mathf.Clamp(cashRate, 0.05f, 0.25f);
                moneyNeeded--;
                PlayerHandler.Instance.RemoveCash(1);
                MMVibrationManager.Haptic(HapticTypes.SoftImpact);
                GameObject _cash = Instantiate(PrefabsManager.Instance.CashGivenPrefab, transform.position,
                    Quaternion.identity);
                _cash.GetComponent<Cash>().GoToTarget(this.transform,cashRate * 2f);
                SetMoneyText(moneyNeeded);
               
                if (moneyNeeded <= 0 && !isBought)
                {
                    isBought = true;
                   CallWorker();
                }
            }
        }
    }

    void CallWorker()
    {
        GameObject _tamirciPrefab = PrefabsManager.Instance.WorkerPrefab;
        GameObject _worker = Instantiate(_tamirciPrefab, WorkerSpawnPoint.position, quaternion.identity);
        int vendingRD = UnityEngine.Random.Range(0, GameManager.Instance.brokenVendingMachines.Count);
        _worker.GetComponent<VendingWorker>().SetWorker(GameManager.Instance.brokenVendingMachines[vendingRD],WorkerSpawnPoint);
        MainUIObject.SetActive(true);
        WorkerCallObject.SetActive(false);
       Invoke("ResetMoneyFlow",4f);
    }

    void ResetMoneyFlow()
    {
        moneyNeeded = totalNeeded;
        cashRate = 0.25f;
        SetMoneyText(moneyNeeded);
        MainUIObject.SetActive(false);
        WorkerCallObject.SetActive(true);
        isBought = false;
    }

    public void WorkerFixedTheProblem()
    {
        ResetMoneyFlow();
        SetWrenchAnim(false);
    }
    void SetMoneyText(int _value)
    {
        MoneyText.text = _value.ToString();
    }
}
