using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.UI;

public class Pickup : MonoBehaviour
{
    public int ID;
    private bool isPopped;

    private void Start()
    {
        Invoke("SelfDestroy",18);
    }

    void SelfDestroy()
    {
        PickupHandler.Instance.RemovePickup();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isPopped)
        {
            Debug.Log("Pickup Pop");
            GameManager.Instance.SetPickupPopUp(ID,true);
            isPopped = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && isPopped)
        {

            GameManager.Instance.SetPickupPopUp(0, false);
            isPopped =false;
        }
    ;
    }
}
