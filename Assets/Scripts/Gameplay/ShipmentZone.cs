using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering.UI;
using UnityEngine.UI;

public class ShipmentZone : MonoBehaviour
{

    [SerializeField] private Transform vehicle;
    [SerializeField] private Transform vehicleAwayPoint, vehicleShipmentPoint;
    [SerializeField] private List<Transform> vehicleBoxesPoint = new List<Transform>();
    [SerializeField] private List<GameObject> shipmentBoxObjects = new List<GameObject>();
    [SerializeField] private  List<Transform> zoneBoxesPoint = new List<Transform>();
    private bool isPoppedUp = false;
    public int boxCount;
    private Transform player;
    public int  playerDistanceToCashFlow ,playerDistanceToPopUp;
    private bool isBought;
    private bool canPlayerCollectItems;
    public GameObject UIObject;
    public GameObject NotBoughtObject, ShipmentComingObject;
    public Transform UIPoint;
    public TextMeshProUGUI MoneyText;
    [SerializeField] private int maxMoneyNeed;
    [SerializeField] private GameObject UpgradeZone;
    [SerializeField] private GameObject tutorialObject;
    private int moneyNeeded;
    private int totalNeeded;
    private float cashRate;
    private float nextMoney;
    private float nextBox;
    private float nextHaptic;

    private void Awake()
    {
        DOTween.Init();
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        moneyNeeded = maxMoneyNeed;
        SetMoneyText(maxMoneyNeed);
        boxCount = 6;
        UpgradeZone.SetActive(false);
    }
    
    void CallShipment()
    {
        isBought = true;
        vehicle.transform.position = vehicleAwayPoint.position;
        vehicle.DOMove(vehicleShipmentPoint.position, 3);
        vehicle.DOShakeScale(5, 0.1f, 15, 1);
        NotBoughtObject.SetActive(false);
        ShipmentComingObject.SetActive(true);
        SpawnBoxesInVehicle();
        Invoke("ShipmentBoxDropTimer",3.25f);
    }

    void SpawnBoxesInVehicle()
    {
        GameObject _shipmentBoxPrefab = PrefabsManager.Instance.ShipmentBoxPrefab;
        
        for (int i = 0; i < boxCount; i++)
        {
            GameObject _shipmentBox =
                Instantiate(_shipmentBoxPrefab, vehicleBoxesPoint[i].position, quaternion.identity,vehicleBoxesPoint[i]);
            shipmentBoxObjects.Add(_shipmentBox);
        }
    }
    
    void ShipmentBoxDropTimer()
    {
        StartCoroutine(ShipmentBoxDrop());
    }

    IEnumerator ShipmentBoxDrop()
    {
        ShipmentComingObject.SetActive(false);
        WaitForSeconds wait = new WaitForSeconds(0.05f);
        for (int i = 0; i < boxCount ; i++)
        {
            shipmentBoxObjects[i].transform.SetParent(null);
            shipmentBoxObjects[i].transform.DOMove(zoneBoxesPoint[i].position, 0.15f);
            yield return wait;
        }
        
        SetCanPlayerCollect(true);
        vehicle.transform.DOMove(vehicleAwayPoint.position, 3);
    }

    void SetCanPlayerCollect(bool _isTrue)
    {
        canPlayerCollectItems = _isTrue;
        UpgradeZone.SetActive(true);
    }

    void SetNoOrder()
    {
        moneyNeeded = maxMoneyNeed;
    }

    private void LateUpdate()
    {
       HandleUI();
        HandlePlayerDistance();
    }
    
    void HandleUI()
    {
        Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
        UIObject.transform.position = UIPos;
    }

    void HandlePlayerDistance()
    {
        if (Time.frameCount % 2 == 0)
        {
            float distance = Vector3.Distance(player.position, transform.position);
         
            if (distance < 7)
            {
                HandleDistancePopUp(distance);
                HandleDistanceCashFlow(distance);
                HandleDistancePlayerCollect(distance);
            }
        }
    }
    
    
    void HandleDistanceCashFlow(float distance)
    {
        if (distance < playerDistanceToCashFlow && moneyNeeded <= PlayerHandler.Instance.cash && moneyNeeded>0 && !isBought &&  !canPlayerCollectItems)
        {
            if (Time.time > nextMoney)
            {
                nextMoney = cashRate + Time.time;
            
                cashRate = cashRate / 1.5f;
                cashRate = Mathf.Clamp(cashRate, 0.05f, 0.25f);
                moneyNeeded--;
                PlayerHandler.Instance.RemoveCash(1);
                MMVibrationManager.Haptic(HapticTypes.SoftImpact);
                GameObject _cash = Instantiate(PrefabsManager.Instance.CashGivenPrefab, transform.position,
                    Quaternion.identity);
                _cash.GetComponent<Cash>().GoToTarget(this.transform,cashRate * 2f);
               SetMoneyText(moneyNeeded);
               
                if (moneyNeeded <= 0 && !isBought)
                {
                    tutorialObject.SetActive(false);
                    CallShipment();
                }
            }
        }
    }
    void HandleDistancePopUp(float distance)
    {
        if (distance < playerDistanceToPopUp && !isPoppedUp)
        {
            isPoppedUp = true;
            UIObject.transform.DOScale(new Vector3(1, 1, 1), 0.25f);
        }
        else if(distance >= playerDistanceToPopUp && isPoppedUp)
        {
            isPoppedUp = false;
            UIObject.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.25f);
        }
    }

    void HandleDistancePlayerCollect(float distance)
    {
        if (distance < playerDistanceToCashFlow && isBought && canPlayerCollectItems && shipmentBoxObjects.Count>0)
        {
            if (Time.time > nextBox)
            {
                nextBox = 0.15f + Time.time;
                if (PlayerHandler.Instance.stackedBoxes.Count < PlayerHandler.Instance.maximumStackCount)
                {
                    PlayerHandler.Instance.StackABox(shipmentBoxObjects[shipmentBoxObjects.Count-1]);
                    shipmentBoxObjects.Remove(shipmentBoxObjects[shipmentBoxObjects.Count - 1]);
                    if (shipmentBoxObjects.Count == 0)
                    {
                        ResetShipment();
                    }
                }
            }
        }
    }

    void ResetShipment()
    {
        moneyNeeded = maxMoneyNeed;
        totalNeeded = maxMoneyNeed;
        SetMoneyText(moneyNeeded);
        vehicle.DOShakeScale(5, 0.1f, 15, 1);
        NotBoughtObject.SetActive(true);
        ShipmentComingObject.SetActive(false);
        isBought = false;
        canPlayerCollectItems = false;
    }

    void SetMoneyText(int _value)
    {
        MoneyText.text = _value.ToString();
    }

    public void UpgradeBoxCount()
    {
        boxCount += 2;
    }
}
