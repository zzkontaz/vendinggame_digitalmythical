using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.UI;
using UnityEngine.UI;

public class VendingMachine : MonoBehaviour
{
    [SerializeField] private int vendingType;
      [SerializeField] private int vendingItemID;
    public int vendingMachineID;
    [SerializeField] private MeshRenderer vendingRenderer;
    public int maxStack;
    public int currentStack;
    public int unlockedParts;
    private Transform player;
    [SerializeField] private CashZone cashZone;
    [SerializeField] private Transform[] cashZonePoint;
    [SerializeField] private float playerDistanceToPopUp,playerDistanceToItemFill;
    private bool isPoppedUp, isVendingItem;
    [SerializeField] private TextMeshProUGUI stackText;

    [SerializeField] private GameObject WarningObject;
    [SerializeField] private Image BoxImg;
    [SerializeField] private Image FillingImage;
    [SerializeField] private GameObject UIObject;
    [SerializeField] private Transform UIPoint;
    [SerializeField] private GameObject MainUIObject;
    [SerializeField] private Transform customerPoint;
    [SerializeField] private CustomerManager customerManager;
    [SerializeField] private bool isBroken;
    [SerializeField] private float brokeCountDown;
    [SerializeField] private float brokeTimer;
    [SerializeField] private GameObject brokenUIObject;
    [SerializeField] private GameObject brokenVfx;
    private void Awake()
    {
        DOTween.Init();
    }

    private void Start()
    {
       Init();
       SetBrokeCountdown();
    }

    void Init()
    {
        GameObject _vfxPrefab = PrefabsManager.Instance.VendingSpawnVFX;
        GameObject _vfx = Instantiate(_vfxPrefab, transform.position, Quaternion.identity);
        player = GameObject.FindGameObjectWithTag("Player").transform;
        GameManager.Instance.AddVendingMachine(this);
        if (vendingType != 3)
        {
            SetStackText(currentStack);
        }
     
        customerManager.SetVendingPoint(customerPoint);
        customerManager.InitVendingMachine(this);
        BoxImg.sprite = GameManager.Instance.vendingMachineItemSprites[vendingItemID];
        WarningObject.SetActive(false);
    }
    
    void SetBrokeCountdown()
    {
        brokeCountDown = UnityEngine.Random.Range(50, 80);
        brokeTimer = 0;
        isBroken = false;
        brokenUIObject.SetActive(false);
        brokenVfx.SetActive(false);
        customerManager.SetMachineBroke(false);
        MainUIObject.SetActive(true);
    }

    private void HandleBrokeCountdown()
    {
        if (Time.frameCount % 2 == 0)
        {
            brokeTimer += 1 * Time.deltaTime;
            if (brokeTimer >= brokeCountDown && !isBroken)
            {
                isBroken = true;
                BrokeVendingMachine();
            }
        }
     
    }

    void BrokeVendingMachine()
    {
        isBroken = true;
        brokenUIObject.SetActive(true);
        brokenVfx.SetActive(true);
        WarningObject.SetActive(false);
        MainUIObject.SetActive(false);
        customerManager.SetMachineBroke(true);
        GameManager.Instance.SetBrokenMachines(true,this);
        TamirZone.Instance.SetWrenchAnim(true);
        PlayerHandler.Instance.SetWaypoint(TamirZone.Instance.transform,true);
        GameManager.Instance.SetTutorialText("Call The Worker For Fixing The Machines",true);
        
    }

    public void VendingMachineFixed()
    {
        if (isBroken)
        {
            Debug.Log("FIXED MACHINE");
            isBroken = false;
            brokenUIObject.SetActive(false);
            brokenVfx.SetActive(false);
            MainUIObject.SetActive(true);
            customerManager.SetMachineBroke(false);
            if (currentStack <= 0)
            {
                WarningObject.SetActive(true);
            }
            SetBrokeCountdown();
            GameManager.Instance.SetBrokenMachines(false,this);
        }
     
    }
    void HandleUI()
    {
        Vector3 UIPos = Camera.main.WorldToScreenPoint(UIPoint.position);
        UIObject.transform.position = UIPos;
        
        if (Time.frameCount % 3 == 0)
        {
            UIObject.SetActive(!GameManager.Instance.isSecondCameraActive);
        }
    }
  

    private void LateUpdate()
    {
        HandleUI();
        HandlePlayerDistance();
        HandleBrokeCountdown();
    }

    void HandlePlayerDistance()
    {
        if (Time.frameCount % 2 == 0)
        {
            float distance = Vector3.Distance(player.position, transform.position);
         
            if (distance < 10)
            {
                HandleDistancePopUp(distance);
                if (vendingType != 3)
                {
                    HandleDistanceItemFilling(distance);
                }
               
            }
        }
    }
    
    void HandleDistancePopUp(float distance)
    {
        if (distance < playerDistanceToPopUp && !isPoppedUp)
        {
            isPoppedUp = true;
            UIObject.transform.DOScale(new Vector3(1, 1, 1), 0.25f);
        }
        else if(distance >= playerDistanceToPopUp && isPoppedUp)
        {
            isPoppedUp = false;
            UIObject.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 0.25f);
        }
    }

    void HandleDistanceItemFilling(float distance)
    {
        if (distance < playerDistanceToItemFill && !isVendingItem)
        {
            FillingImage.fillAmount += 2 * Time.deltaTime;
          
        }
        else if(distance >= playerDistanceToItemFill)
        {
            FillingImage.fillAmount -= 5 * Time.deltaTime;
           
        }
        
        if (FillingImage.fillAmount ==1 && !isVendingItem && PlayerHandler.Instance.stackedBoxes.Count>0  && !isBroken)
        {
            if (vendingType == 0)
            {
                if (currentStack < PlayerHandler.Instance.stackedBoxes.Count * 2) // EDIT
                {
                    UIObject.SetActive(false);
                    if (vendingType == 0)
                    {
                        VendingFillingHandler.Instance.StartItemFilling(vendingItemID,currentStack,vendingMachineID);
                        VendingFillingHandler.Instance.UnlockZones(unlockedParts);
                    }
                    else if (vendingType == 1)
                    {
                        CandyFillingHandler.Instance.StartItemFilling(vendingItemID,currentStack,vendingMachineID);
                    }
                    else if (vendingType == 2)
                    {
                        ToyFillingHandler.Instance.StartItemFilling(vendingItemID,currentStack,vendingMachineID);
                    }
                    isVendingItem = true;
                    PlayerHandler.Instance.MaxStackObject.SetActive(false);
                } 
            }
            else
            {
                UIObject.SetActive(false);
                if (vendingType == 0)
                {
                    VendingFillingHandler.Instance.StartItemFilling(vendingItemID,currentStack,vendingMachineID);
                    VendingFillingHandler.Instance.UnlockZones(unlockedParts);
                }
                else if (vendingType == 1)
                {
                    CandyFillingHandler.Instance.StartItemFilling(vendingItemID,currentStack,vendingMachineID);
                }
                else if (vendingType == 2)
                {
                    ToyFillingHandler.Instance.StartItemFilling(vendingItemID,currentStack,vendingMachineID);
                }
                isVendingItem = true;
                PlayerHandler.Instance.MaxStackObject.SetActive(false);
            }
            
          
            
        }
        
    }
    
    
   public void SetID(int _vendingType,int _item, int _machine,bool _isRightCash)
   {
       vendingType = _vendingType;
        vendingItemID = _item;
        vendingMachineID = _machine;
        currentStack = 0;
        if (vendingType == 0)
        {
            vendingRenderer.material = GameManager.Instance.vendingMachineMaterials[vendingItemID];
            maxStack = 24;
        }
        else if(vendingType ==1)
        {
            maxStack = 120;
        }
        else if(vendingType == 2)
        {
            maxStack = 48;
        }
        else if(vendingType == 3)
        {
            maxStack = 999;
            for (int i = 0; i < 500; i++)
            {
                AddItemToVendingMachine();
            }
          
        }

   
        transform.DOPunchScale(Vector3.up, 0.5f, 2);
        if (!_isRightCash)
        {
            cashZone.gameObject.transform.position = cashZonePoint[0].position;
        }
        else
        {
            cashZone.gameObject.transform.position = cashZonePoint[1].position;
        }
    }
   
   public void OnValidate()
   {
       HandleUI();
   }

   public void AddItemToVendingMachine()
   {
       currentStack++;
       if (currentStack > maxStack) currentStack = maxStack;
       SetStackText(currentStack);
     
   }

   public void CustomerBoughtItem()
   {
      
       if (vendingType == 0 || vendingType == 2)
       { 
           currentStack--;
           cashZone.AddCash(2);
       }
       else if (vendingType == 1)
       {
           currentStack -= 5;
           if (currentStack < 0) currentStack = 0;
           cashZone.AddCash(1);
       }
       
       SetStackText(currentStack);
       transform.DOPunchScale(new Vector3(0, 0.25f, 0), 0.25f);
       Invoke("ResetMachineScale",0.35f);
     
       if (currentStack <= 0)
       {
           currentStack = 0;
           CustomerDespawn();
           WarningObject.SetActive(true);
       }
   }

   void ResetMachineScale()
   {
       transform.localScale = new Vector3(1,1,1);
   }

   public void ItemsLoadFinish()
   {
       isVendingItem = false;
       Invoke("SetUIObjectOn",2.25f);
       Invoke("SetUIObjectOn",2.25f);
       Invoke("SetCustomerSpawn",0.25f);
       WarningObject.SetActive(false);
   }

   void SetUIObjectOn()
   {
       UIObject.SetActive(true);
   }

   public void SetCustomerSpawn()
   {
       customerManager.canCustomerSpawn = true;
   }

   void CustomerDespawn()
   {
       customerManager.canCustomerSpawn = false;
   }

   void SetStackText(int _value)
   {
       stackText.text = _value.ToString();
   }

   public void RemoveItem()
   {
       currentStack--;
       if (currentStack < 0) currentStack = 0;
       SetStackText(currentStack);
   }

   public void UnlockAZone()
   {
       unlockedParts++;
   }
}
