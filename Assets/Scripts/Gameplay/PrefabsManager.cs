using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsManager : MonoBehaviour
{
    public static PrefabsManager Instance;
    
    public GameObject CashPrefab;
    public GameObject CashGivenPrefab;
    public GameObject CustomerPrefab;
    public GameObject[] VendingMachinePrefabs;
    public GameObject[] CandyPrefabs;
    public GameObject ShipmentBoxPrefab;
    public GameObject[] pickupPrefabs;
    public GameObject VendingSpawnVFX;
    public GameObject WorkerPrefab;
    public GameObject[] ItemPrefabs;
    public GameObject[] ToyPrefabs;
   /*
    * 0 SU -mavi
    * 1 MEYVE SUYU - yeşil
    * 2 SARI CİPS - sarı
    * 3 KOLA - kırmızı
    * 4 PRINGLES - turuncu
    * 5 FANTA - turuncu
    * 6 DONUT -pembe
    * 7 MİLKA -mor
    */
    
    private void Awake()
    {
        Instance = this;
    }
}
