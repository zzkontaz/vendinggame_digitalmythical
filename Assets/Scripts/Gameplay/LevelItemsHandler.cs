using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelItemsHandler : MonoBehaviour
{
   public List<GameObject> VendingMachines = new List<GameObject>();
   private int vendingMachineOrder = 0;

   public static LevelItemsHandler Instance;

   private void Awake()
   {
      Instance = this;
   }

   public void AddVendingMachineToScene()
   {
      if (VendingMachines[vendingMachineOrder] != null)
      {
         if (!VendingMachines[vendingMachineOrder].activeSelf)
         {
            VendingMachines[vendingMachineOrder].SetActive(true);
            vendingMachineOrder++;
            if (vendingMachineOrder > VendingMachines.Count - 1) vendingMachineOrder = VendingMachines.Count - 1;
         }
      }
     
      

   }
}
