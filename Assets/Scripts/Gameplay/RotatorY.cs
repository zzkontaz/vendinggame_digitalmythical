using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorY : MonoBehaviour
{
    [SerializeField] private int rotSpeed;

    private void LateUpdate()
    {
        transform.Rotate(transform.up * rotSpeed * Time.deltaTime);
    }
}
